package br.com.caelum.eats.pedido;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static java.util.Optional.ofNullable;

@RestController
@AllArgsConstructor
class PedidoController {

    private PedidoRepository pedidoRepository;

    @GetMapping("/pedidos")
    List<PedidoDto> lista() {
	return pedidoRepository.findAll().stream().map(PedidoDto::new).collect(Collectors.toList());
    }

    @GetMapping("/pedidos/{id}")
    PedidoDto porId(@PathVariable("id") Long id) {
	Pedido pedido = pedidoRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
	return new PedidoDto(pedido);
    }

    @PostMapping("/pedidos")
    PedidoDto adiciona(@RequestBody Pedido pedido) {
	pedido.setDataHora(LocalDateTime.now());
	pedido.setStatus(Pedido.Status.REALIZADO);
	pedido.getItens().forEach(item -> item.setPedido(pedido));
	pedido.getEntrega().setPedido(pedido);
	Pedido salvo = pedidoRepository.save(pedido);
	return new PedidoDto(salvo);
    }

    @PutMapping("/pedidos/{id}/status")
    PedidoDto atualizaStatus(@RequestBody Pedido pedido) {
	pedidoRepository.atualizaStatus(pedido.getStatus(), pedido);
	return new PedidoDto(pedido);
    }

    @GetMapping("/parceiros/restaurantes/{restauranteId}/pedidos/pendentes")
    List<PedidoDto> pendentes(@PathVariable("restauranteId") Long restauranteId) {
	return pedidoRepository.doRestauranteSemOsStatus(restauranteId, Arrays.asList(Pedido.Status.REALIZADO, Pedido.Status.ENTREGUE))
			.stream().map(PedidoDto::new).collect(Collectors.toList());
    }

    @PutMapping("/pedidos/{id}/pago")
    void pago(@PathVariable("id") Long id) {
	Pedido pedido = ofNullable(pedidoRepository.porIdComItens(id)).orElseThrow(ResourceNotFoundException::new);
	pedido.setStatus(Pedido.Status.PAGO);
	pedidoRepository.atualizaStatus(Pedido.Status.PAGO, pedido);
    }

}
