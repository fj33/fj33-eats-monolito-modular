package br.com.caelum.eats.restaurante;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

import static br.com.caelum.eats.restaurante.RestauranteUtil.cepDiferente;
import static br.com.caelum.eats.restaurante.RestauranteUtil.tipoDeCozinhaDiferente;

@RestController
@AllArgsConstructor
class RestauranteController {

    private RestauranteRepository restauranteRepository;
    private CardapioRepository    cardapioRepository;
    private DistanciaRestClient   distanciaRestClient;

    @GetMapping("/restaurantes/{id}")
    RestauranteDto detalha(@PathVariable("id") Long id) {
	Restaurante restaurante = restauranteRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
	return new RestauranteDto(restaurante);
    }

    @GetMapping("/parceiros/restaurantes/do-usuario/{username}")
    public RestauranteDto detalhaParceiro(@PathVariable("username") String username) {
	Restaurante restaurante = restauranteRepository.findByUsername(username);
	return new RestauranteDto(restaurante);
    }

    @GetMapping("/restaurantes")
    List<RestauranteDto> detalhePorIds(@RequestParam("ids") List<Long> ids) {
	return restauranteRepository.findAllById(ids).stream().map(RestauranteDto::new).collect(Collectors.toList());
    }

    @GetMapping("/parceiros/restaurantes/{id}")
    RestauranteDto detalhaParceiro(@PathVariable("id") Long id) {
	Restaurante restaurante = restauranteRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
	return new RestauranteDto(restaurante);
    }

    @PostMapping("/parceiros/restaurantes")
    Restaurante adiciona(@RequestBody Restaurante restaurante) {
	restaurante.setAprovado(false);
	Restaurante restauranteSalvo = restauranteRepository.save(restaurante);
	Cardapio cardapio = new Cardapio();
	cardapio.setRestaurante(restauranteSalvo);
	cardapioRepository.save(cardapio);
	return restauranteSalvo;
    }

    @PutMapping("/parceiros/restaurantes/{id}")
    Restaurante atualiza(@RequestBody Restaurante restaurante) {
	Restaurante restauranteDb = restauranteRepository.getOne(restaurante.getId());
	restaurante.setUser(restauranteDb.getUser());
	restaurante.setAprovado(restauranteDb.getAprovado());
	Restaurante restauranteSalvo = restauranteRepository.save(restaurante);

	if (restaurante.getAprovado() && (cepDiferente(restaurante, restauranteDb) || tipoDeCozinhaDiferente(restaurante, restauranteDb))) {
	    distanciaRestClient.restauranteAtualizado(restaurante);
	}

	return restauranteSalvo;
    }

    @GetMapping("/admin/restaurantes/em-aprovacao")
    List<RestauranteDto> emAprovacao() {
	return restauranteRepository.findAllByAprovado(false).stream().map(RestauranteDto::new).collect(Collectors.toList());
    }

    @Transactional
    @PatchMapping("/admin/restaurantes/{id}")
    public void aprova(@PathVariable("id") Long id) {
	restauranteRepository.aprovaPorId(id);
    }

}
