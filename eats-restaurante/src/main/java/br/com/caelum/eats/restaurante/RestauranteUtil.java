package br.com.caelum.eats.restaurante;

public final class RestauranteUtil {

    private RestauranteUtil(){}

    static boolean tipoDeCozinhaDiferente(Restaurante restaurante, Restaurante restauranteDb) {
	return !restauranteDb.getTipoDeCozinha().getId().equals(restaurante.getTipoDeCozinha().getId());
    }

    static boolean cepDiferente(Restaurante restaurante, Restaurante restauranteDb) {
	return !restauranteDb.getCep().equals(restaurante.getCep());
    }

}
